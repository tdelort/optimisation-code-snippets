# Setup
import Pkg
Pkg.add("JuMP")
using JuMP
using GLPK
# might need to press "y" to install GLPK

# Declaring model/problem
m = Model(GLPK.Optimizer)
#=
A JuMP Model
Feasibility problem with:
Variables: 0
Model mode: AUTOMATIC
CachingOptimizer state: EMPTY_OPTIMIZER
Solver name: GLPK
=#

@variable(m, 42 <= xa <= 100)
# xa
@variable(m, 53 <= xb <= 100)
# xb

@objective(m, Max, 450 * xa + 800 * xb)
# 450 xa + 800 xb

@constraint(m, 1.5 * xa + 2 * xb <= 250)
# 1.5 xa + 2 xb <= 250.0
@constraint(m, 0.5 * xa + 0.75 * xb <= 100)
# 0.5 xa + 0.75 xb <= 100.0
@constraint(m, 2 * xa + 3 * xb <= 327)
# 2 xa + 3 xb <= 327.0

JuMP.optimize!(m)

print(JuMP.value(xa))
# 42.0
print(JuMP.value(xb))
# 81.0

JuMP.objective_value(m)
# 83700.0
