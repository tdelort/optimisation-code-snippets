### Setup


```python
pip install pulp
```

    Requirement already satisfied: pulp in c:\anaconda3\lib\site-packages (2.6.0)
    Note: you may need to restart the kernel to use updated packages.
    


```python
from pulp import *
```

### Declare Problem


```python
m = LpProblem(name="Nintendo", sense=LpMaximize)
```

Variables


```python
np = LpVariable(name="np", lowBound=42, upBound=100)
ns = LpVariable(name="ns", lowBound=53, upBound=100)
```

Objectif


```python
m += (450 * np + 800 * ns, "obj")
```

Contraintes


```python
m += (1.5 * np + 2 * ns <= 250, "c1")
m += (0.5 * np + 0.75 * ns <= 100, "c2")
m += (2 * np + 3 * ns <= 327, "c3")
```

### Solve


```python
m.solve()
print(f"Np = {np.varValue}")
print(f"Ns = {ns.varValue}")
print(f"Profit : {value(m.objective)}")
```

    Np = 42.0
    Ns = 81.0
    Profit : 83700.0
    
