% tests
doc isoutlier
a = 2

% Prepare data
maison = DataUSACart(:,13)
maison = table2array(maison)

amputation = DataUSACart(:, 11)
amputation = table2array(amputation)

% draw plots
plot(maison, "*")
plot(amputation, "*")

% draw both plots on same canvas
plot(maison, "+")
hold on
plot(amputation, "*")

% smooth curve
maison_lisse = smooth(maison)
plot(maison_lisse)
